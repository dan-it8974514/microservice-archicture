const PRODUCTS_SERVICE = "http://localhost:3000/";
const CART_SERVICE = "http://localhost:3001/";
const ORDER_SERVICE = "http://localhost:3002/";
const PAY_SERVICE = "http://localhost:3003/";

const clientEmulation = async () => {
    const products = await fetch(PRODUCTS_SERVICE);
    const { products: productsJson } = await products.json();

    console.log("productsJson: ", productsJson)

    await fetch(CART_SERVICE, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            userId: 1,
            productId: productsJson[0].id
        })
    })

    await fetch(CART_SERVICE, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            userId: 1,
            productId: productsJson[2].id
        })
    })

    const cartResponse = await fetch(CART_SERVICE + "?userId=1");
    const { cart } = await cartResponse.json()

    console.log("cart: ", cart)

    const generateOrder = await fetch(ORDER_SERVICE + '1', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
    })

    const generatedOrder = await generateOrder.json()

    console.log("generatedOrder: ", generatedOrder)

    const getOrder = await fetch(ORDER_SERVICE + generatedOrder.id);

    const order = await getOrder.json();

    console.log("orderData: ", order)

    const transactionResponse = await fetch(PAY_SERVICE + 'create-transaction', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ order })
    })

    const transactionData = await transactionResponse.json();

    console.log("transactionData: ", transactionData)

    const payResponse = await fetch(PAY_SERVICE + 'pay', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            cardNumber: 4444444444444444,
            transactionId: transactionData.id,
            cvv: 123,
            expDate: "01/25",
            successHook: `${ORDER_SERVICE}hook/success/${order.id}`,
            failureHook: `${ORDER_SERVICE}hook/failure/${order.id}`
        })
    })

    const payData = await payResponse.json();

    console.log("payData: ", payData)
}

clientEmulation()