module.exports = {
    getCartKey: (userId) => {
        return `cart:user:${userId}`;
    },
}