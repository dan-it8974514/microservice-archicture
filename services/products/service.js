const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const path = require("path");
const knex = require("knex")({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, '..', '..', 'db.sqlite'),
    },
    useNullAsDefault: true
})

const app = express();

app.use(bodyParser.json());
app.use(morgan("dev"));

app.get("/", async (req, res) => {
    const products = await knex("products").select('id', 'name', 'price');

    console.log("products: ", products)

    res.json({ products })
})

app.listen(3000, async () => {
    const isDatabaseHasProductsTable = await knex.schema.hasTable('products');

    if (!isDatabaseHasProductsTable) {
        await knex.schema.createTable('products', (table) => {
            table.increments('id');
            table.string('name');
            table.integer('price');
            table.timestamps();
        })


        await knex('products').insert([
            {
                id: 1,
                name: "Coffee",
                price: 100,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                id: 2,
                name: "Tea",
                price: 200,
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                id: 3,
                name: "Chips",
                price: 300,
                created_at: new Date(),
                updated_at: new Date()
            }
        ])
    }

    console.log("Product service listening on port 3000")
});