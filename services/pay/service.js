const express = require("express");
const morgan = require("morgan");
const path = require("path");
const bodyParser = require("body-parser");
const knex = require("knex")({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, '..', '..', 'db.sqlite'),
    },
    useNullAsDefault: true
})

const axios = require("axios");

const app = express();

app.use(morgan("dev"));
app.use(bodyParser.json());

app.post("/create-transaction", async (req, res) => {
    const { order } = req.body;

    const tax = 0.05 * order.total;

    const transaction = {
        order,
        total: order.total + tax,
        status: "pending",
        created_at: new Date(),
        updated_at: new Date()
    }

    const [id] = await knex("transactions").insert(transaction);

    res.json({ id })
})

const users = [
    {
        id: 1,
        name: "John",
        age: 25,
        balance: 5000,
        cardNumber: 4444_4444_4444_4444
    }
]

app.post('/pay', async (req, res) => {
    const { cardNumber, transactionId, cvv, expDate, successHook, failureHook } = req.body;

    const transaction = await knex('transactions').where('id', transactionId).where('status', 'pending').first();

    if (!transaction) {
        return res.status(404).json({
            message: "Transaction not found"
        })
    }

    const user = users.find(user => user.cardNumber === cardNumber);

    if (!user) {
        return res.status(404).json({
            message: "User not found"
        })
    }

    if (user.balance < transaction.total) {
        await axios.post(failureHook, {
            orderId: transaction.order.id
        })

        return res.status(400).json({
            message: "Insufficient balance"
        })
    }

    user.balance -= transaction.total;

    await knex('transactions').where('id', transactionId).update({ status: 'completed' });
    await axios.post(successHook, {
        orderId: transaction.order.id
    })

    res.json({ user })
})

app.listen(3003, async () => {
    const isDatabaseHasTransactionsTable = await knex.schema.hasTable('transactions');

    if (!isDatabaseHasTransactionsTable) {
        await knex.schema.createTable('transactions', (table) => {
            table.increments('id');
            table.json('order');
            table.integer('total');
            table.enum('status', ['pending', 'completed', 'canceled']);
            table.timestamps();
        })
    }

    console.log("Pay service listening on port 3003")
})