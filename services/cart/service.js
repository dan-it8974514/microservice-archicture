const express = require("express");
const bodyParser = require("body-parser");
const redis = require("redis");
const morgan = require("morgan");
const redisKeys = require("../../redis-keys");

const app = express();

const client = redis.createClient();

app.use(bodyParser.json());
app.use(morgan("dev"));

app.get("/", async (req, res) => {
    const { userId } = req.query;

    console.log("req.query: ", req.query)
    console.log("req.params: ", req.params)

    const cart = await client.get(`cart:user:${userId}`);

    if (!cart) {
        return res.json({
            cart: []
        })
    }

    res.json({
        cart: JSON.parse(cart)
    })
})

app.post("/", async (req, res) => {
    const { userId, productId } = req.body;

    let cart = await client.get(`cart:user:${userId}`);
    cart = JSON.parse(cart)

    if (!cart) {
        cart = []
    }

    console.log("cart: ", cart)

    if (cart.includes(productId)) {
        return res.json({
            cart
        })
    }

    cart.push(productId)

    const pipe = client.multi()

    const userCartKey = redisKeys.getCartKey(userId)
    pipe.set(userCartKey, JSON.stringify(cart)).expire(userCartKey, 60 * 30).exec()

    res.json({
        cart
    })
})

app.listen(3001, async () => {
    await client.connect();

    client.on('error', (err) => {
        console.error(`Redis client not connected: ${err}`);
    });

    console.log("Cart service listening on port 3001");
});