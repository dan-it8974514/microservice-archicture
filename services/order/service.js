const express = require("express");
const bodyParser = require("body-parser");
const redis = require("redis");
const morgan = require("morgan");
const path = require("path");
const redisKeys = require("../../redis-keys");
const knex = require("knex")({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, '..', '..', 'db.sqlite'),

    },
    useNullAsDefault: true
})

const app = express();
const client = redis.createClient();

app.use(bodyParser.json());
app.use(morgan("dev"));

app.get("/:orderId", async (req, res) => {
    const { orderId } = req.params;

    const order = await knex("orders").where("id", orderId).first();

    res.json(order)
})

app.post('/hook/success/:orderId', async (req, res) => {
    const { orderId } = req.params;

    if (!orderId) {
        return res.status(400).json({
            success: false
        })
    }

    console.log('ORDER COMPLETED: ', orderId)
    await knex('orders').where('id', orderId).update({ status: 'completed' });


    res.json({
        success: true
    })
})

app.post('/hook/failure/:orderId', async (req, res) => {
    const { orderId } = req.params;

    if (!orderId) {
        return res.status(400).json({
            success: false
        })
    }

    console.log('CANCELLING ORDER: ', orderId)

    await knex('orders').where('id', orderId).update({ status: 'canceled' });

    res.json({
        success: false
    })
})

app.post("/:userId", async (req, res) => {
    const { userId } = req.params;

    const userCartKey = redisKeys.getCartKey(userId);

    const cart = await client.get(userCartKey);

    if (!cart) {
        return res.json({
            message: "Cart is empty",
            orderInfo: null
        })
    }

    const products = await knex("products").whereIn("id", JSON.parse(cart));

    const order = {
        userId,
        orderInfo: {
            products
        },
        total: products.reduce((total, product) => total + product.price, 0),
        status: "pending",
        created_at: new Date(),
        updated_at: new Date()
    }

    const [id] = await knex("orders").insert(order);

    res.json({
        id,
        ...order,
    })
})

app.listen(3002, async () => {
    await client.connect();

    client.on('error', (err) => {
        console.error(`Redis client not connected: ${err}`);
    });

    const isDatabaseHasOrderTable = await knex.schema.hasTable('orders');

    if (!isDatabaseHasOrderTable) {
        await knex.schema.createTable('orders', (table) => {
            table.increments('id');
            table.integer('userId');
            table.json('orderInfo');
            table.enum('status', ['pending', 'completed', 'canceled']);
            table.integer('total');
            table.timestamps();
        })
    }

    console.log("Cart service listening on port 3002");
});