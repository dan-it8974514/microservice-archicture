const path = require("path");
const knex = require("knex")({
    client: 'sqlite3',
    connection: {
        filename: path.join(__dirname, '..', 'db.sqlite'),
    },
    useNullAsDefault: true
})

const cancelOrders = async () => {
    const orders = await knex('orders').where("status", "pending")

    console.log('PENDING ORDERS: ', orders)

    for (const order of orders) {
        if (Date.now() - order.created_at > 1000 * 60 * 24) {
            await knex('orders').where('id', order.id).update({ status: 'canceled' })
        }
    }

}

const main = async () => {
    await cancelOrders();
    setInterval(cancelOrders, 1000 * 60);
}

main()